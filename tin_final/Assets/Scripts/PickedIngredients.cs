﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickedIngredients : MonoBehaviour
{
    public static PickedIngredients PI;
    public List<Ingredients> wanted;



    // Start is called before the first frame update
    void Start()
    {
        if (PI != null)
        {
            Destroy(PI);
        }

        PI = this;
        DontDestroyOnLoad(gameObject);
        //wanted = new List<Ingredients>();
    }

    
}
