﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecipeBook : MonoBehaviour
{
    public List<RecipeClass> allRecipes;
    // Start is called before the first frame update
    void Start()
    {
        

        List<RecipeClass> matches = FindRecipes(PickedIngredients.PI.wanted);

        for (int i=0; i < matches.Count; i++)
        {
            print(i + "." + matches[i].dishName);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    List<RecipeClass> FindRecipes(List<Ingredients> wanted)
    {
        List<RecipeClass> matches = new List<RecipeClass>();
        for (int i = 0; i < allRecipes.Count; i++)
        {
            int score = 0;

            for (int h = 0; h < allRecipes[i].ingredients.Count; h++)
            {
                if (wanted.Contains(allRecipes[i].ingredients[h])) {
                    score++;
                }

            }
            allRecipes[i].score = score;
            if (score > 0)
                matches.Add(allRecipes[i]);
        }
        matches.Sort();
        return matches;
    }

}
