﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum Ingredients {
    onion, garlic, cilantro, apple, oats, butter, tumeric, potato, egg, flour, brownSugar, salt, vanilla,
    curryPowder, cayenne, cumin, pepper, sugar, bakingPowder, milk, mayonnaise, whiteVinegar, yellowMustard,
    paprika, anyMeat, chiliPowder, tomoatoPaste, garlicPowder, meatBroth, dicedTomatoes, beans, chicken, cornstarch,
    buffaloHotSauce, riceVinegar, chiliFlakes, greenOnion, fettuccine, whiteMushrooms, heavyCream, romanoCheese,
    italianParsley, riceVermicelli, riceWrappers, anySeafood, thaiBasil, lettuce, fishSauce, lime, chickenWings,
    chipotleSeasoning, anyBarbecueSauce, honey
    
};



[System.Serializable]
public class RecipeClass : IComparable<RecipeClass>
{
    public string dishName;
    public List<Ingredients> ingredients;
    [HideInInspector]
    public int score;

    public int CompareTo(RecipeClass other)
    {
        return other.score - score;
    }
    
}
