﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class UIManager : MonoBehaviour
{
    
    public Animator dialog;
    public InputField protein;
    public InputField veggies;
    public InputField fruits;
    public InputField saucePowders;
    public InputField other;
    public Text fText;
    
    public Animator WildOptionButton;


    public void OpenSettings()
    {

        WildOptionButton.SetBool("isHidden", true);

        dialog.SetBool("isHidden", false);
    }

    public void CloseSettings()
    {

        WildOptionButton.SetBool("isHidden", false);
        dialog.SetBool("isHidden", true);
    }



    public void setget()
    {
        int total = Int32.Parse(protein.text) + Int32.Parse(veggies.text) +
            Int32.Parse(fruits.text) + Int32.Parse(saucePowders.text) + Int32.Parse(other.text);
        fText.text = total.ToString() + " total calories";

    }


    public void StartGame()
    {
        SceneManager.LoadScene("CreateScene");
    }

    public void StartGame2()
    {
        SceneManager.LoadScene("BreakfastThirdScene");
    }

    public void StartGame3()
    {
        SceneManager.LoadScene("ResultScene");
    }


    public void StartGame5()
    {
        SceneManager.LoadScene("StartScene");
    }

    public void StartGame6()
    {
        SceneManager.LoadScene("ResultScene");
    }

    public void StartGame7()
    {
        SceneManager.LoadScene("CalorieScene");
    }

}
